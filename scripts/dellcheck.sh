#!/bin/sh

cleanup() {
    if [ -e $clog ]; then
        rm $clog
    fi
}

simple_check() {
sudo /opt/MegaRAID/MegaCli/MegaCli64 -LDInfo -Lall -aALL -NoLog 2>&1 >> $clog
if [ $(cat $clog | grep -i 'State\|Permission' | grep -v Optimal |wc -l) -eq 0 ]; then
echo 0
else
echo 1
fi
}

#RUN
clog=$(mktemp /var/tmp/clog.XXXXXXXXXXXX)
simple_check
cleanup
